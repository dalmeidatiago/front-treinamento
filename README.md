# Front-end do projeto 1 - Treinamento SIGA 2024

## Dupla
Gabriel Ferreira Leão e Tiago Gonçalves Ferreira Rodrigues d'Almeida

## Descrição

Esse repositório é a implementação do front end do primeiro projeto do processo seletivo da equipe de desenvolvimento do SIGA (UFRJ) de 2024. É uma página simples, que fornece uma visualização amigável para a manipulação de dados em um banco de dados SQL que contém registros de alunos. O banco é acessado através de uma API disponível nesse [link](https://gitlab.com/dalmeidatiago/treinamento-do-zero).

A página permite acessar todas as rotas da API (exceto a de buscar um aluno individualmente), detalhadas na descrição do repositório desta. Imediatamente ao abrir a página, é feita uma requisição para a rota ```/aluno``` da API com o método GET. Cada ação que manipule dados dos alunos dispara outras chamadas a essa API para que a mudança seja de fato realizada no banco de dados, e não apenas na visualização.

Por padrão, a página exibe todos os registros de alunos, facilitando a visualização dos dados, e evitando por exemplo que o usuário tenha que saber a matrícula de um aluno à priori para poder alterar ou consultar seus dados. Nessa mesma visualização, são dispostos de forma intuitiva os botões que fazem acesso às outras rotas da API para manipulação dos dados. As ações permitidas são: Criar, apagar, cancelar a matrícula e editar as informações de um aluno.

Para garantir a sincronia dessas mudanças com o banco de dados, o retorno de cada chamada é checado, e caso ocorra um erro de qualquer tipo, este é informado ao usuário de forma intuitiva. Nesse sentido, são tomadas precauções para que os dados que o usuário veja sejam de fato os registrados no banco.

## Setup e uso
Para que a página exiba os dados, é necessário que o backend esteja rodando na porta 8080. Para isso, se ainda não o tiver feito, siga as orientações do README desse [outro repositório](https://gitlab.com/dalmeidatiago/treinamento-do-zero).

Após isso, para iniciar a aplicação, baixe ou clone esse repositório (front), e rode os comandos:
```shell
# Entrar no diretório
cd treinamento-dev-front
# Instalar dependencias
npm i
# Iniciar o servidor
npm run dev
```

A página então estará disponível em ```http://localhost:5173/```, como a própria saída do npm deve orientar.

### Operações

#### Criar aluno
Para criar um aluno, há um botão de "+" acima do primeiro registro de aluno. Clicando nele, surgirá um novo card com os campos editáveis. Se o usuário clicar em cancelar, o card é descartado, e se clicar em salvar, é enviada uma requisição para criar o usuário. Os dados inseridos devem respeitar as restrições delimitadas na API. Sendo elas:

- Nome deve ter 255 caracteres ou menos
- Hobbies deve ter 255 caracteres ou menos
- Matrícula deve ter exatamente 9 caracteres
- Não deve haver um aluno existente com a mesma matrícula

Caso uma dessas condições não seja respeitada ou haja outro erro na requisição, um card vermelho aparecerá indicando o erro.

#### Editar aluno
Em cada card de aluno há um botão azul que diz "editar". Clicando nele, os campos do card tornam-se editáveis, com uma indicação visual clara dessa mudança. Similarmente ao processo de criação, ao clicar em salvar, uma requisição é enviada para alterar o aluno, com os dados inseridos devendo respeitar os mesmos critérios, e ao clicar em cancelar, o card volta à forma original, não editável. Em caso de falha ao salvar, o mesmo card de erro aparece, mas com outra mensagem, coerente com o erro.

#### Apagar aluno
Há também em cada card um botão vermelho que diz "deletar". Clicando nele, é enviada uma requisição contendo a matrícula do aluno selecionado e solicitando a sua deleção. Se a API estiver operante e de fato existir um aluno com essa matrícula, a operação é realizada e a UI é atualizada, removendo esse aluno da visualização. Caso contrário, uma mensagem de erro similar aparece.

#### Cancelar matrícula
O último botão nos cards é o amarelo que diz "cancelar". Clicando nele, uma requisição é automaticamente preenchida com os dados do aluno selecionado solicitando uma edição, mas dessa vez, alterando o campo de situação de matrícula, visando alterá-lo para "C01" (cancelado). Novamente, qualquer erro é informado ao usuário.